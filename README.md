# Binary Neural Network for Image Classification

A binary neural network consists of only 2 representation (+1 and -1) for weights or activation function representation. This makes the complex multiplication and addition to simple bitwise or logical operations.